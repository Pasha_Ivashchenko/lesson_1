
function setBackgroundColor() {
    var randomColor = getRandomIntInclusive();
    document.body.style.backgroundColor = randomColor;
    showColor(randomColor);
    function getRandomIntInclusive(min, max) {
        return "#" + Math.random().toString(16).slice(2, 8);
    };
    function showColor(randomColor) {
        var showColorDiv = document.createElement('div');
        showColorDiv.setAttribute('id','showColorDiv');
        showColorDiv.style.cssText = "background-color: white;" +
            "width: 63px;" +
            "position: relative;" +
            "left: 50%;" +
            "transform: translate(-50%, 50%)";
        showColorDiv.style.color = randomColor;
        showColorDiv.innerText = randomColor;
        showColorDiv.style.marginTop = '15px';
        document.body.appendChild(showColorDiv);
    };
};

function setNewHeader(){
    var header = document.createElement('header');
    header.style.marginTop = '20px';

    var anchor_1 = document.createElement('a');
    anchor_1.setAttribute('href','http://google.com.ua');
    var image = document.createElement('img');
    image.setAttribute('src','https://www.google.com.ua/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png');
    anchor_1.appendChild(image);
    header.appendChild(anchor_1);

    var div = document.createElement('div');
    div.className = 'menu';
    for(var i = 0; i < 3; i++){
        var anchorMult = document.createElement('a');
        anchorMult.setAttribute('href','#');
        anchorMult.innerText = ' link ' + ( i + 1 );
        anchorMult.style.display = 'block';
        div.appendChild(anchorMult);
    }
    header.appendChild(div);

    document.body.appendChild(header);
};


/*
  Задание 1.

  Написать функцию которая будет задавать СЛУЧАЙНЫЙ цвет для фона.
  Каждая перезагрузка страницы будет с новым цветом.
  Для написания используйте функцию на получение случайного целого числа,
  между минимальным и максимальным значением (Приложена снизу задания)

  + Бонус, повесить обработчик на кнопку через метод onClick
  + Бонус, использовать 6-ричную систему исчесления и цвет HEX -> #FFCC00
  + Бонус выводить полученый цвет по центру страницы.
  Необходимо создать блок через createElement задать ему стили через element.style
  и вывести через appendChild или insertBefore


  Необходимые материалы:
    Math.Random (Доки): https://developer.mozilla.org/uk/docs/Web/JavaScript/Reference/Global_Objects/Math/random
    function getRandomIntInclusive(min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    __
    Работа с цветом:
    Вариант 1.
      Исользовать element.style.background = 'rgb(r,g,b)';
      где r,g,b случайное число от 0 до 256;

    Вариант 2.
      Исользовать element.style.background = '#RRGGBB';
      где, RR, GG, BB, значние цвета в 16-ричной системе исчесления
      Формирование цвета в вебе: https://ru.wikipedia.org/wiki/%D0%A6%D0%B2%D0%B5%D1%82%D0%B0_HTML
      Перевод в 16-ричную систему исчесления делается при помощи
      метода Number.toString( 16 ) https://www.w3schools.com/jsref/jsref_tostring_number.asp,

      var myNumber = '251'
      myNumber.toString(16) // fb

*/

/*
    Задание 2.

    Сложить в элементе с id App следующую размету HTML:

    <header>
      <a href="http://google.com.ua">
        <img src="https://www.google.com.ua/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png">
      </a>
      <div class="menu">
        <a href="#1"> link 1</a>
        <a href="#1"> link 2</a>
        <a href="#1"> link 3</a>
      </div>
    </header>


    Используя следующие методы для работы:
    getElementById
    createElement
    element.innerText
    element.className
    element.setAttribute
    element.appendChild

*/