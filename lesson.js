/*

  invite to telegram chat: https://t.me/joinchat/CvyJfRFi38U0fvaAx5XJMg

  BOM ->
  window.
        navigator, https://developer.mozilla.org/ru/docs/Web/API/Navigator
        history, https://developer.mozilla.org/ru/docs/Web/API/History
        screen, https://developer.mozilla.org/ru/docs/Web/API/Screen
        location, https://developer.mozilla.org/ru/docs/Web/API/Location
        document -> DOM https://developer.mozilla.org/ru/docs/Web/API/Document
        alert, prompt, console,

*/

// console.log('Hi console');
// console.table({name: 'value1'});
// console.table(["apples", "oranges", "bananas"]);
// console.error('Some error in obj:', { name: 'Cat'})
// console.warn(' SOME WARNING');

// console.log("This is the outer level");
// console.group();
// console.log("Level 2");
// console.group();
// console.log("Level 3");
// console.warn("More of level 3");
// console.groupEnd();
// console.log("Back to level 2");
// console.groupEnd();
// console.log("Back to the outer level");

// console.time('fetch');
// var fetchedData = fetch('http://www.json-generator.com/api/json/get/bQnzYgqhua?indent=2').then(function(response) {
//  return response.json();
// }).then(data => {
//    console.timeEnd('fetch');
//    console.log('data', data);
// });

// console.log( navigator );
// console.log( location);
// location.href = "http://google.com.ua";
// console.log( location.hash);
// console.log( screen.width, screen.height );
//
// window.history.pushState({message: 'wow'}, '', '#link1');
//
// numberOfEntries = window.history.length;
// console.log(numberOfEntries);
// console.log( 'message before', window.history.state.message );
//
//
// window.history.pushState({message: 'BOOM'}, '', '#link2');
// console.log( 'message after', window.history.state.message );


//
// for (var i = 0; i < document.body.childNodes.length; i++) {
//     console.log( document.body.childNodes[i] ); // Text, DIV, Text, UL, ..., SCRIPT
//   }
//
//



/*

  DOM нужен для того, чтобы манипулировать страницей –
  читать информацию из HTML, создавать и изменять элементы.

  Всё, что есть в HTML, находится и в DOM.

  document.getElementById -> в контекстке document

  Возращают колекцию могут быть вызваны в контексте
  как документа как и любого элемента

  element.getElementsByTagName
  element.getElementsByClassName

  element.querySelectorAll(css) -> где css любой css selector, вернет колекцию
  element.querySelector(css) -> вернет первое совпадение
  element.matches(css) -> проверка, удовлетворяет ли селектору css, возвращает boolen

  element.closest(css) -> находит ближйший элемент вверх по иерархии DOM который удовлетвореят css

  *document.getElementsByName(name)
*/

// document.body.style.background = 'red';
// Выбрать элемент по id и применить стиль
// id="user" создает переменую user в глобальном обьекте
// console.log( user );
// document.getElementById('user').style.background = 'red';
// user.style.background = 'green';

// Выбрать элементы по тегу li -> в элементе с id=List
// var listItems = document.getElementById('list').getElementsByTagName('li');
//     console.log( typeof(listItems), listItems[1].innerText );

// Выборка по css селектору
// var listItems = document.getElementById('list').querySelectorAll('*');
// console.log( listItems );
// Проверка элемента по селектору
// console.log( 'matches', listItems[0].matches('li') );
// closest
// var ClosestItems = document.getElementById('JackLi');
// console.log( 'closest', ClosestItems.closest('ul'));

// Выбрать элементы по аттрибуту name
// var nameItem = document.getElementsByName('Dexter');
//     console.log( 'name',  nameItem );



/*
  ATTR
  element.hasAttribute(name) – проверяет наличие атрибута
  element.getAttribute(name) – получает значение атрибута
  element.setAttribute(name, value) – устанавливает атрибут
  element.removeAttribute(name) – удаляет атрибут
  element.attributes - получить все атрибуты
  element.dataset - > получить data-attr
*/


/*
  CREATE ELEMENT
  document.createElement(tag) – создает элемент
  document.createTextNode(value) – создает текстовый узел

  PASTE ELEMENT
  parent.appendChild(element)
  parent.insertBefore(element, nextSibling)

  REMOVE ELEMENT
  parentElement.removeChild(element);
  element.remove();
*/

// var div = document.createElement('div');
// // var textElem = document.createTextNode('Тут был я');
//     div.className = "message";
//     div.innerHTML = "status";

// var createArea = document.getElementById('createArea');
//     createArea.appendChild(div);
//     createArea.insertBefore(textElem, createArea.children[1]);

// var deletedElement = document.getElementsByClassName('message');
//     console.log('deletedElement', deletedElement);
//     // createArea.removeChild(deletedElement[0]);
//     deletedElement[0].remove();